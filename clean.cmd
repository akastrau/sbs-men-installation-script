@echo off
echo.
echo Czyszcze pliki tymczasowe...

echo T | DEL /F /S /Q O:\desktop\PoziomA
echo T | DEL /F /S /Q O:\desktop\PoziomB
echo T | DEL /F /S /Q O:\desktop\PoziomC
echo T | DEL /F /S /Q O:\desktop\PoziomD
echo T | DEL /F /S /Q O:\menus\PoziomA
echo T | DEL /F /S /Q O:\menus\PoziomB
echo T | DEL /F /S /Q O:\menus\PoziomC
echo T | DEL /F /S /Q O:\menus\PoziomD
echo T | DEL /F /S /Q O:\profiles\
echo T | RMDIR /S /Q O:\redirected
mkdir O:\redirected\
echo T | RMDIR /S /Q O:\desktop\PoziomA
echo T | RMDIR /S /Q O:\desktop\PoziomB
echo T | RMDIR /S /Q O:\desktop\PoziomC
echo T | RMDIR /S /Q O:\desktop\PoziomD
echo T | RMDIR /S /Q O:\memenus\
echo T | RMDIR /S /Q O:\menus\PoziomA
echo T | RMDIR /S /Q O:\menus\PoziomB
echo T | RMDIR /S /Q O:\menus\PoziomC
echo T | RMDIR /S /Q O:\menus\PoziomD
echo T | RMDIR /S /Q O:\profiles
mkdir O:\profiles
rd /S /Q %systemroot%\Setup
rd /S /Q %systemroot%\status
echo T | DEL /F /Q C:\DOMAIN.cmd
echo T | DEL /F /Q C:\PRNTYPE.cmd
echo T | DEL /F /Q C:\SUM.cmd
echo T | DEL /F /Q C:\Unattend.txt
echo T | DEL /F /S /Q %systemroot%\Temp
echo.
echo.
call delay.exe 5

echo.
echo Instalacja podstawowych elementow zakonczona!
echo T | DEL "C:\Documents and Settings\All Users\Menu Start\Programy\Autostart\*"
echo.
call delay.exe 3
exit