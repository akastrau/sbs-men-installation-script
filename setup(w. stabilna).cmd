@echo off
echo A. Kastrau 2013
echo Skrypt instalacyjny SBS 2000 v. 1.45
echo =====================================
echo.

if not exist %systemroot%\status\* goto crdir
:crdir
mkdir %systemroot%\status
if exist %systemroot%\status\init.txt (
goto init)

:main

echo Konfiguracja wstepna...
echo. >> %systemroot%\status\init.txt
echo T | xcopy /E /Y /Q D:\i386\$oem$\$$\*.* %systemroot%\

echo.
goto init

:init
echo Ustawiam zmienne...
call %systemroot%\Setup\setvar.cmd 

xcopy %systemroot%\Setup\setup.cmd "C:\Documents and Settings\All Users\Menu Start\Programy\Autostart"

if not exist %systemroot%\status\* goto crdir
goto dcinit


:dcinit

if exist %systemroot%\status\dcstart.txt ( 
goto sbs2k )

echo Trwa uruchamianie instalatora uslugi Active Directory...
echo. >> %systemroot%\status\dcstart.txt
call dcpromo.exe
Echo Czekaj na restart!
pause
:sbs2k

if exist %systemroot%\status\sbs2k.txt (
goto prepart)
echo. >> %systemroot%\status\sbs2k.txt
echo Trwa uruchamianie instalatora SBS 2000...
echo.
echo ==========================================
echo Przekopiuj klucz produktu SBS 2000:
echo h6twq-tqqm8-hxjyg-d69f7-r84vm
echo.
echo Przekopiuj klucz produktu Outlook:
echo bcqbk-fmhh3-q6w44-f79cd-8mwcd
echo ==========================================
call %cdrom%\BKOFFICE\i386\Setup.exe
echo.
echo Jesli instalator poprosi o restart, zrestartuj komputer!
echo Nie naciskaj dowolnego klawisza dopoki nie zainstaluje sie SBS!
echo.
echo Instalacja zostanie dokonczona po restarcie!
echo.
echo.

rem Wykrylem problem z kopiowaniem z okna konsoli
rem Powoduje on natychmiastowe rozpoczecie formatowania (tak nie powinno by�)
rem Dlatego plik z kluczami b�dzie zapisany na pulpicie.
rem Z niego bedzie mo�na kopiowa� klucz bez obaw.

cd C:\Documents*\
cd Administrator\Pulpit
echo Przekopiuj klucz produktu SBS 2000: >> klucze.txt
echo h6twq-tqqm8-hxjyg-d69f7-r84vm >> klucze.txt
echo. >> klucze.txt
echo Przekopiuj klucz produktu Outlook: >> klucze.txt
echo bcqbk-fmhh3-q6w44-f79cd-8mwcd >> klucze.txt
cd ../
cd ../
cd %winroot%\Setup

:prepart
if exist %systemroot%\status\reboot.txt (
goto clean)

:part
pause
echo Czekaj formatuje partycje....
cd %systemroot%\Setup\part
call part.cmd
call format.exe o: /FS:NTFS /V:Dane

cls

cd ../
cd shares

echo Tworze foldery u�ytkownikow....
echo.
call shares.cmd
call shares2.cmd
pause
cls

echo Konfiguruje serwer DHCP...
echo.
cd ../
cd dhcp
call dhcpsrv.cmd

cls

cd ../
echo Konfiguruje ISA Server...
echo.
cd ISAcfg
call isacfg.cmd
cls

echo Tworze konta uzytkownikow...
echo.
cd ../
cd accounts
call im.cmd
call accounts.cmd
call im.cmd
cls

echo Importuje obiekty zasad grup...
echo.
cd ../
cd GPO
call scrzip.cmd
call loadpol.cmd

cd ../
call wscript check.vbs D:\CD!.txt CD1
echo. >> %systemroot%\status\reboot.txt
call shutdown /L /R /T:15 /C "Automatyczne czyszczenie nastapi po restarcie!" 
pause

cls
:clean
start D:\I386\$oem$\$$\Setup\clean.cmd
exit
