@echo off
echo A. Kastrau 2013
echo Skrypt instalacyjny SBS 2000 v. 1.63
echo =====================================
echo.

rem =============================================
rem +++++++++++++++++++++++++++++++++++++++++++++
rem Wymagania potrzebne do wykonania skryptu 
rem instalacyjnego:
rem 1) P�yta SBS MEN 2000 z plikiem CD!.txt
rem w katalogu g��wnym,
rem 
rem 2) Plik check.exe* w katalogu $OEM$\$$\Setup\
rem * Plik wykonywalny mojego autorstwa
rem
rem 3) Zmodyfikowany plik instalacji nienadzorowanej
rem (unattend.txt)
rem
rem 4) Plik delay.exe jest wymagany, mozna uzyc zamiast niego waitfor
rem z tym ze nalezy zmienic skrypt przed jego uruchomieniem!
rem
rem UWAGA! Obraz p�yty nr 1 zosta� przeze mnie
rem troch� zmodyfikowany (usuni�te katalogi 
rem instalacji UPS'a itd.)
rem
rem OPCJONALNIE: setcd.cmd w katalogu $OEM\$$\Setup\
rem
rem Skrypt wykonany w celu u�atwienia instalacji SBS'a
rem w pracowni szkolnej
rem
rem                11111011101
rem ++++++++++++++++++++++++++++++++++++++++++++++
rem ==============================================
 


if not exist %systemroot%\status\* goto crdir else goto main 
:crdir
mkdir %systemroot%\status

:main
if exist %systemroot%\status\init.txt (
goto init)

echo Konfiguracja wstepna...
echo. >> %systemroot%\status\init.txt
echo T | xcopy /E /Y /Q D:\i386\$oem$\$$\*.* %systemroot%\

echo.
goto init

:init
if exist %systemroot%\status\vars2.txt (
goto predcinit)

echo Ustawiam zmienne...
call %systemroot%\Setup\setvar.cmd
call %systemroot%\Setup\setcd.cmd 

echo T | xcopy /Q %systemroot%\Setup\setup.cmd "C:\Documents and Settings\All Users\Menu Start\Programy\Autostart\*"
echo. >> %systemroot%\status\vars.txt
call delay.exe 5
:predcinit

goto dcinit


:dcinit

if exist %systemroot%\status\dcstart.txt ( 
goto sbs2k )

echo.
echo Trwa uruchamianie instalatora uslugi Active Directory...
echo. >> %systemroot%\status\dcstart.txt
call dcpromo.exe
echo Czekaj na restart!
pause
:sbs2k
if exist %systemroot%\status\vars.txt (
echo. >> %systemroot%\status\vars2.txt)
if exist %systemroot%\status\sbs2k.txt (
goto prepart)
echo. >> %systemroot%\status\sbs2k.txt
echo.
echo Trwa uruchamianie instalatora SBS 2000...
echo.
echo ==========================================
echo Przekopiuj klucz produktu SBS 2000:
echo h6twq-tqqm8-hxjyg-d69f7-r84vm
echo.
echo Przekopiuj klucz produktu Outlook:
echo bcqbk-fmhh3-q6w44-f79cd-8mwcd
echo ==========================================
call %cdrom%\BKOFFICE\i386\Setup.exe
echo.
echo Jesli instalator poprosi o restart, zrestartuj komputer!
echo Nie naciskaj dowolnego klawisza dopoki nie zainstaluje sie SBS!
echo.
echo Instalacja zostanie dokonczona po restarcie!
echo.
echo.

rem Wykry�em problem z kopiowaniem z okna konsoli
rem Powoduje on natychmiastowe rozpocz�cie formatowania (tak nie powinno by�)
rem Dlatego plik z kluczami b�dzie zapisany na pulpicie.
rem Z niego b�dzie mo�na kopiowa� klucz bez obaw.

cd C:\Documents*\
cd Administrator\Pulpit
echo Przekopiuj klucz produktu SBS 2000: >> klucze.txt
echo h6twq-tqqm8-hxjyg-d69f7-r84vm >> klucze.txt
echo. >> klucze.txt
echo Przekopiuj klucz produktu Outlook: >> klucze.txt
echo bcqbk-fmhh3-q6w44-f79cd-8mwcd >> klucze.txt
cd ../
cd ../

call delay.exe 300

:prepart
if exist %systemroot%\status\isasp1.txt (
goto clean)

:part
if exist %systemroot%\status\post.txt (
goto extended)

call delay.exe 15
echo Czekaj formatuje partycje....
cd %systemroot%\Setup\part
call part.cmd
call format.exe o: /FS:NTFS /V:Dane

cls

cd ../
cd shares

echo Tworze foldery u�ytkownikow....
echo.
call shares.cmd
call shares2.cmd
call delay.exe 5
cls

echo Konfiguruje serwer DHCP...
echo.
cd ../
cd dhcp
call dhcpsrv.cmd

cls

cd ../
echo Konfiguruje ISA Server...
echo.
cd ISAcfg
call isacfg.cmd
cls

echo Tworze konta uzytkownikow...
echo.
cd ../
cd accounts
call accounts.cmd
call im.cmd
echo. 
cls
call delay.exe 5
echo.
echo Trwa dodawanie uzytkownikow do grupy BackOffice Internet Users...
echo.
echo AUTG.vbs, Jim Roberts 2006, ver. 1.1
echo =========================================
echo.
call autg "BackOffice Internet Users" internet.txt C:\log.txt /di
echo.
call delay.exe 5
echo.
echo Ustawiam skrypty logowania netlogon...
call mogrp.vbs LDAP://cn=Users,dc=men,dc=edu,dc=pl /i:adm.txt
echo.
call delay.exe 5

echo Importuje obiekty zasad grup...
echo.
cd ../
cd GPO
call scrzip.cmd
call loadpol.cmd

echo.
echo Ustawiam strone startowa servera IIS...
echo T | copy %systemroot%\Setup\www\Default.htm C:\InetPub\wwwroot\
echo T | del C:\InetPub\wwwroot\iisstart*
echo.
echo.

echo Ustawiam skrypt NETLOGON...
echo T | copy %systemroot%\Setup\net\* %systemroot%\SYSVOL\sysvol\men.edu.pl\scripts\
echo.

echo. >> %systemroot%\status\post.txt
cd ../

rem ----------------------------------------
rem Cz�� rozszerzona (SP i inne hotfixy)
rem ----------------------------------------
:extended

:hotfix
echo.
if exist %systemroot%\status\hotfix.txt (
goto ie6setup)
echo Instaluje Hotfixy...
call %systemroot%\Setup\check.exe D:\ClientApps5\IE60\ie6setup.exe CD6
echo. >> %systemroot%\status\hotfix.txt
D:
cd Client*\
cd hot*\
call setup.exe /Q
echo.

:ie6setup
cd ../
cd ../
if exist %systemroot%\status\ie6setup.txt (
goto updates)
echo Instaluje IE6...
echo. >> %systemroot%\status\ie6setup.txt
D:
cd Client*\
cd IE*\
call ie6setup.exe /Q
pause

:updates
if exist %systemroot%\status\updates.txt (
goto win2ksp1)
echo.
cd %systemroot%\Setup\HotFix\
call hotfix.cmd
echo. >> %systemroot%\status\updates.txt
cd ../

:win2ksp1
if exist %systemroot%\status\sp4setup.txt (
goto isasp1setup)
echo Instaluje SP4 dla Windows 2000...
call %systemroot%\Setup\check.exe D:\CD10.txt CD10
echo. >> %systemroot%\status\sp4setup.txt
cd ../
cd ../
D:
call w2ksp4_pl.exe -q
pause

:isasp1setup
if exist %systemroot%\status\isasp1.txt (
goto clean)
echo Instaluje SP1 ISA Server...
call %systemroot%\Setup\check.exe D:\isasp1\isasp1.exe CD9
echo. >> %systemroot%\status\isasp1.txt
D:
cd isa*
call isasp1.exe -q
pause


cls
:clean
call %systemroot%\Setup\check.exe D:\CD!.txt CD1
start D:\I386\$oem$\$$\Setup\clean.cmd
exit
